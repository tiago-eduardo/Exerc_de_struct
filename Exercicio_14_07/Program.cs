﻿using System;
using System.Net.Http.Headers;

namespace Exercicio_14_07
{
    class Program
    {
        public struct Aluno
        {
            public string nome;
            public int idade;
            public double media;


        }
        static void Main(string[] args)
        {
     
            Aluno aluno1, aluno2, aluno3;

            Console.WriteLine("Digite o nome do 1° aluno:");
            aluno1.nome = Console.ReadLine();

            Console.WriteLine("Digita a idade do 1° aluno:");
            aluno1.idade = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite a media do 1° aluno:");
            aluno1.media = double.Parse(Console.ReadLine());

            Console.WriteLine("Digite o nome do 2° aluno:");
            aluno2.nome = Console.ReadLine();

            Console.WriteLine("Digita a idade do 2° aluno:");
            aluno2.idade = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite a media do 2° aluno:");
            aluno2.media = double.Parse(Console.ReadLine());

            Console.WriteLine("Digite o nome do 3° aluno:");
            aluno3.nome = Console.ReadLine();

            Console.WriteLine("Digita a idade do 3° aluno:");
            aluno3.idade = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite a media do 3° aluno:");
            aluno3.media = double.Parse(Console.ReadLine());

            Console.Write("\n\n{0} tem {1} anos de idade e ficou com {2:n2} de média", aluno1.nome, aluno1.idade, aluno1.media);
            Console.Write("\n\n{0} tem {1} anos de idade e ficou com {2:n2} de média", aluno2.nome, aluno2.idade, aluno2.media);
            Console.Write("\n\n{0} tem {1} anos de idade e ficou com {2:n2} de média", aluno3.nome, aluno3.idade, aluno3.media);

            Console.ReadKey();
        }
    }
}
